﻿#ps1_sysnative





Param([string]$DomainUser="",[boolean]$CreateCluster=$false,[boolean]$AddToExistingCluster=$false,[string]$ClusterIP = "");


start-transcript -path "C:\Openstack_log.txt";

# OPENSTACK



$user = [ADSI]'WinNT://./Administrator';
$user.SetPassword('administrator_password');





# Configuration

$gitpass= 'openstack_git_pass';
$o365user= 'openstack_o365_user';
$o365password= 'openstack_o365_password';

$PSproxyGit = "https://396462:$($gitpass)@gitlab.ics.muni.cz/396462/PSwebProxy.git";
$PSWSGit =  "https://396462:$($gitpass)@gitlab.ics.muni.cz/240494/PSWS.git";
$PSWSmsmqBackendGit =  "https://396462:$($gitpass)@gitlab.ics.muni.cz/396462/o365msmqCommunication.git";



$ErrorActionPreference = 'Continue';

# Configure network

$netConfig = Get-NetIPConfiguration
$IP = $netConfig.ipv4address.ipaddress;
$MaskBits = $netConfig.ipv4address.PrefixLength;
$Gateway = $netConfig.IPv4DefaultGateway.nexthop;
$Dns = $netConfig.dnsserver.serveraddresses[0];
$IPType = "IPv4";

# Retrieve the network adapter that you want to configure
$adapter = Get-NetAdapter | ? {$_.Status -eq "up"};

# Remove any existing IP, gateway from our ipv4 adapter
If (($adapter | Get-NetIPConfiguration).IPv4Address.IPAddress) {
 $adapter | Remove-NetIPAddress -AddressFamily $IPType -Confirm:$false;
};
If (($adapter | Get-NetIPConfiguration).Ipv4DefaultGateway) {
 $adapter | Remove-NetRoute -AddressFamily $IPType -Confirm:$false;
};
 # Configure the IP address and default gateway
$adapter | New-NetIPAddress -AddressFamily $IPType -IPAddress $IP -PrefixLength $MaskBits -DefaultGateway $Gateway;
$adapter | Set-DnsClientServerAddress -ServerAddresses $DNS;


Start-Sleep -Seconds 10;


# Create predefined local user if no other user is specified

if($DomainUser -eq ""){
    $PswsUser = "PSRestAPIuser";
    $PswsUserPassword = "administrator_password";
    NET USER $PswsUser $PswsUserPassword /add;
}else{
    $PswsUser = $DomainUser;
};

# Add new user to local administrators

NET LOCALGROUP Administrators $PswsUser /add;


# Install Chocolatey

iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'));

# Upgrade Powershell to 5.0 if necessary (restart is required)

if((Get-Host).Version.Major -lt 5){
     choco install powershell -y;
};

# Install Azure AD module and requirements
Install-PackageProvider -Name NuGet -Force;
install-module msonline -Force;
import-module msonline -Force;


# Install git

choco install git -y ;


# Install required features - IIS + ODATA extension + MSMQ + NLB + MSOnline

Install-WindowsFeature -Name "Web-Server" -IncludeAllSubFeature -IncludeManagementTools -ErrorAction Stop;
    
Install-WindowsFeature -Name "msmq" -IncludeAllSubFeature -IncludeManagementTools -ErrorAction Stop;

Install-WindowsFeature -Name "nlb" -IncludeAllSubFeature -IncludeManagementTools -ErrorAction Stop;


# Configure NLB

if(($createCluster -eq $true) -and ($clusterIP -ne "")){

    New-NlbCluster -InterfaceName $netConfig.InterfaceAlias -ClusterPrimaryIP $clusterIP -ClusterName psws -operationMode multicast;

};

if(($addToExistingCluster -eq $true) -and ($ClusterIP -ne "")){
    
    Get-NlbCluster $ClusterIP | Add-NlbClusterNode -NewNodeName $env:COMPUTERNAME -NewNodeInterface $netConfig.InterfaceAlias;

};


# Clone Source files from repository

cd c:\;
& 'C:\Program Files\Git\bin\git.exe' clone $PSproxyGit;

Start-Sleep -Seconds 5;


# Configure IIS service

cd C:\PSwebProxy\setup\;

.\SetupEndpoint.ps1;



# Configure PS proxy background scripts

cd C:\;

& 'C:\Program Files\Git\bin\git.exe' clone -b DEV $PSWSGit;

Start-Sleep -Seconds 5;

Import-Module C:\PSWS\PSWS_BackEnd\PSWSMuni.psd1;



# Create MSMQ queues

New-MsmqQueue -Journaling:$true -Name o365 -JournalQuota 1024 -Label "o365 inbound" -ErrorAction Stop;
    
New-MsmqQueue -Journaling:$true -Name o365result -JournalQuota 1024 -Label "o365 outbound" -ErrorAction Stop;

Get-MsmqQueue -Name o365 -QueueType Private -ErrorAction Stop | Set-MsmqQueueAcl -UserName $PswsUser -Allow FullControl -ErrorAction Stop;

Get-MsmqQueue -Name o365result -QueueType Private -ErrorAction Stop | Set-MsmqQueueAcl -UserName $PswsUser -Allow FullControl -ErrorAction Stop;


# Create PSWS msmq backend

cd C:\;
& 'C:\Program Files\Git\bin\git.exe' clone $PSWSmsmqBackendGit;

Start-Sleep -Seconds 5;

cd C:\o365msmqCommunication;

#.\GenerateSecureCredentials.ps1

# Read-Host "Press enter to exit..."

stop-transcript;

exit 1001; 